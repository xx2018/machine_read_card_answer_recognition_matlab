%function XuanZe3
clc;%清除命令行
close all;%关闭figure
clear;%清除变量

%%
I = imread('jiaozang.jpg');%较脏
% I = imread('img3.png');
%判断是否彩图
if size(I,3) > 1 % 维度 ndims(I) > 2
    I = rgb2gray(I);
end
I = im2double(I);

%% 
%二值化 - 第一天
% T = adaptthresh(I,0.4,'ForegroundPolarity','dark');
% BW = imbinarize(I,T);
% BW = ~BW;


avgI = averagefilter(I, floor(size(I)/16)*2+1, 'replicate');
I_s  = 1 - avgI + I;%
I_s  = I_s.^16;%一定要用0-1之间的灰度
%I_s=I_s/2;
BW  = im2bw(I_s,graythresh(I_s));%im2bw
BW = ~BW;
% figure('name','原图二值化'),imshow(BW);

%%
%ROI定位 - 第二天

% %求取边缘
% Ie=edge(I,'canny');
% S1 = regionprops(Ie,'BoundingBox','PixelIdxList');
% max_area = 0;
% for i = 1:length(S1)
%     area = S1(i).BoundingBox(3)*S1(i).BoundingBox(4); 
%     if area>max_area
%        max_area = area;
%        pos = i;
%     end
% end
% bbox = S1(pos).BoundingBox;
% figure('name','提取区域结果'),imshow(I(bbox(2):bbox(2)+bbox(4),bbox(1):bbox(1)+bbox(3)));%小数
% Inew = zeros(size(I));
% Inew(S1(pos).PixelIdxList) = I(S1(pos).PixelIdxList);
% figure('name','求取边缘'),imshow(Inew);

%%
%提取直线
bws = BW;
% [H,theta,rho] = hough(bws);% hough(I,'Theta',0:-0.1:-89.5);
%imshow(H,[]),colormap(hot)
% P=houghpeaks(H,5);
% lines=houghlines(bws,theta,rho,P,'FillGap',5,'MinLength',7);
% figure('name','提取直线');imshow(bws);hold on;
% for k=1:length(lines)
%     xy = [lines(k).point1; lines(k).point2];
%    plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
% 
%    % Plot beginnings and ends of lines
%    plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
%    plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
% end
% hold off
% AngH=cat(1,lines.theta);
%AngkH=kmeans(AngH,2);

% %
% %求线段的交点
% p1=cat(1,lines.point1);
% p2=cat(1,lines.point2);
% pcnt=size(p1,1);
% pselect=[];cnt=0;
% for i=1:pcnt-1
%     for j=i:pcnt
%         %线段为ab，和cd
%         a.x=p1(i,1);
%         a.y=p1(i,2);
%         b.x=p2(i,1);
%         b.y=p2(i,2);
%         c.x=p1(j,1);
%         c.y=p1(j,2);
%         d.x=p2(j,1);
%         d.y=p2(j,2);
%         denominator = (b.y - a.y)*(d.x - c.x) - (a.x - b.x)*(c.y - d.y);
%         if abs(denominator)<200%denominator==0，线段平行
%            continue;
%         end
%         x = ( (b.x - a.x) * (d.x - c.x) * (c.y - a.y)  ... 
%                 + (b.y - a.y) * (d.x - c.x) * a.x   ...
%                 - (d.y - c.y) * (b.x - a.x) * c.x ) / denominator ;  
%         y = -( (b.y - a.y) * (d.y - c.y) * (c.x - a.x)   ...
%                 + (b.x - a.x) * (d.y - c.y) * a.y   ...
%                 - (d.x - c.x) * (b.y - a.y) * c.y ) / denominator;  
%          cnt=cnt+1;
%          pselect(cnt,1:2)=[x y];
%     end
% end
% figure('name','求交点');imshow(bws);hold on;plot(pselect(:,1),pselect(:,2),'r+');hold off

%%
%寻找harris角点      
% corners = detectHarrisFeatures(I);
% imshow(I); hold on;
% plot(corners.selectStrongest(100));

L1 = bwlabel(BW);
S1 = regionprops(L1,'BoundingBox');
max_area = 0;
for i = 1:length(S1)
    area = S1(i).BoundingBox(3)*S1(i).BoundingBox(4); 
    if area>max_area
       max_area = area;
       pos = i;
    end
end
ROIbw = (L1 == pos);

% figure(),imshow(ROIbw);
corners = detectHarrisFeatures(ROIbw);
%  figure('name','harris候选点'),imshow(I); hold on;
%  plot(corners.selectStrongest(79));hold off % 8

hc = corners.selectStrongest(6);
hcxy = hc.Location;
% hcxy = pselect;
 loccir = mean(hcxy);
dis = (hcxy(:,1)-loccir(1)).^2+(hcxy(:,2)-loccir(2)).^2;
[dissort,idx] = sort(dis);
ROIpoints = hcxy(idx(end-3:end),:);
% figure('name','确定角点'),imshow(I); hold on;
% plot(ROIpoints(:,1),ROIpoints(:,2),'r+');hold off;

%%
%根据最大外界矩形框确定范围
%已经得到最大标号 pos
% 
% ROIpoints(1,1) = S1(pos).BoundingBox(1);
% ROIpoints(1,2) = S1(pos).BoundingBox(2);
% 
% ROIpoints(2,1) = S1(pos).BoundingBox(1) + S1(pos).BoundingBox(3);
% ROIpoints(2,2) = S1(pos).BoundingBox(2);
% 
% ROIpoints(3,1) = S1(pos).BoundingBox(1);
% ROIpoints(3,2) = S1(pos).BoundingBox(2) + S1(pos).BoundingBox(4);
% 
% ROIpoints(4,1) = S1(pos).BoundingBox(1) + S1(pos).BoundingBox(3);
% ROIpoints(4,2) = S1(pos).BoundingBox(2) + S1(pos).BoundingBox(4);
% 
% figure('name','确定角点'),imshow(I); hold on;
% plot(ROIpoints(:,1),ROIpoints(:,2),'r+');hold off;
%%
%选择题识别 - 第三天

%R=A/B=inv(A)*B;B=A*R;
disxy(:,1) = ROIpoints(:,1)-loccir(1);
disxy(:,2) = ROIpoints(:,2)-loccir(2);
disflag = disxy<0;
ind = xor(disflag(:,1),disflag(:,2))+disflag(:,2)*2+1;
ROIpoints = ROIpoints(ind,:);

Roibw = roipoly(I,[ROIpoints(:,1);ROIpoints(1,1)],...
    [ROIpoints(:,2);ROIpoints(1,2)]);


RoiImage = I .* Roibw;
% figure('name','ROI 待识别区域'),imshow(RoiImage)

 %四个原顶点
x = ROIpoints(:,2);
y = ROIpoints(:,1);       

width = round(max(x)-min(x));
height= round(max(y)-min(y));
%校正后的顶点
Y(1)  = y(1); Y(4)=y(1);Y(2:3)=Y(1)+height;
X(1:2)= x(1);X(3:4)=X(1)-width;

tform = fitgeotrans(ROIpoints,[Y' X'],'similarity');%similarity Projective
% [newy,newx] = transformPointsForward(tform,ROIpoints(:,1),ROIpoints(:,2));
% del=[newy,newx]-[Y' X'];
Is = imwarp(I,tform); %应用变换,将图像旋转  I为灰度图像，不能二值图
Iover=imfuse(I,Is);
% figure('name','2222'),imshow(Iover);
%Is = imwarp(RoiImage,tform); 
Is = imwarp(bws,tform); %会有空洞
% figure('name','校正-图像'),imshow(Is);

Is=Is(X(3):X(1),Y(1):Y(2));
avgI = averagefilter(Is, floor(size(Is)/16)*2+1, 'replicate');
 I_s  = 1 - avgI + Is;%
 I_s  = I_s.^16;
 bws  = im2bw(I_s,graythresh(I_s));

figure(6);imshow(bws)
%Roibw = roipoly(bws,[Y(1,:) Y(1,1)]',[X(1,:) X(1,1)]');

edgerad=1;
bws(1:edgerad,:)=0;bws(:,1:edgerad)=0;
bws(end-edgerad+1:end,:)=0;bws(:,end-edgerad+1:end)=0;

S2 = regionprops(bws,'BoundingBox','Area');
bwArea=cat(1,S2.Area);
[bwAreasort,idx]=sort(bwArea);
bbx=round(S2(idx(end-1)).BoundingBox);
bwss=bws(bbx(2):bbx(2)+bbx(4),bbx(1):bbx(1)+bbx(3));
figure(7);imshow(bwss)
bwss=~bwss;
bwss(1:edgerad,:)=0;bwss(:,1:edgerad)=0;
bwss(end-edgerad+1:end,:)=0;bwss(:,end-edgerad+1:end)=0;
S2 = regionprops(bwss,'BoundingBox','Area');
bwArea=cat(1,S2.Area);
[bwAreasort,idx]=sort(bwArea);
bbx=round(S2(idx(end)).BoundingBox);%还应加详细的判断
bwsn=bwss(bbx(2):bbx(2)+bbx(4),bbx(1):bbx(1)+bbx(3));
figure(8);imshow(bwsn)

% 
% minareathresh=35;
% bws=bwareaopen(bws,minareathresh);
% imshow(bws);