%function select
clc;%清除命令行
close all;%关闭figure
clear;%清除变量
%%
I=imread('jiaozang.jpg');

[im_w, im_h] = size(I);
% remX = mod(im_w-40,10);
% offsetX = floor(remX/2)+1;
% remY = mod(im_h-40,10);
% offsetY = floor(remY/2)+1;
% [gridY1,gridX1] = meshgrid( offsetY:10:im_h+1,offsetX:10:im_w+1);
% [gridX2,gridY2] = meshgrid(offsetX:10:im_w+1, offsetY:10:im_h+1);
[gridX1,gridY1] = meshgrid( 1:50:im_w,1:100:im_h);
[gridY2,gridX2] = meshgrid(1:50:im_w, 1:100:im_h);
imshow(I);
hold on;
plot(gridX2,gridY2,'g');
plot(gridX1,gridY1,'r');
hold off;
