I=imread('jiaozang.jpg');
[im_h, im_w] = size(I);
remX = mod(im_w-40,10);
offsetX = floor(remX/2)+1;
remY = mod(im_h-40,50);
offsetY = floor(remY/2)+1;
[gridY1,gridX1] = meshgrid( offsetY:10:im_h+1,offsetX:10:im_w+1);
[gridX2,gridY2] = meshgrid(offsetX:20:im_w+1, offsetY:20:im_h+1);
imshow(I);
hold on;
plot(gridX2,gridY2,'r');
plot(gridX1,gridY1,'g');
hold off;
