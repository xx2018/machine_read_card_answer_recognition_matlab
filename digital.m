function [labelp,classp]= digital()
% try 
%     load my1vsall_model
%     load mypara
% catch
clc;

  filepath0 = 'digittest/训练/';%'synthetic/';
  filepaths=dir(filepath0);
  cnt=0;
for i=3:size(filepaths,1)
    filenames=dir([filepath0 filepaths(i).name '/']);
    for j=3:size(filenames,1)
        cnt=cnt+1;
        trainingSet(cnt).Files=[filepath0 filepaths(i).name '/'  filenames(j).name];%设置文件路径
        trainingSet(cnt).Labels=str2double(filepaths(i).name);         %设置标签 - 数字作为标签
    end
end

%Train a Digit Classifier
%训练数字分类
numImages =cnt;
cellSize = [4 4];
img = readimage(trainingSet, 1);%读取图片 并 灰度化
imgsize = size(img);
[hog_4x4, ~] = extractHOGFeatures(img,'CellSize',[4 4]);%HOG特征检测
hogFeatureSize = length(hog_4x4);
trainingFeatures = extracthog(trainingSet,numImages,imgsize,cellSize,hogFeatureSize);%得到HOG特征
% Get labels for each image.
%得到每一张图片的标签
trainingLabels =cat(1, trainingSet.Labels);%将标签 构造数组 ，1为上下拼接

numLabels=10;
%训练一对多分类器
model = cell(numLabels,1);%结构体 可存储不同类型数据
for k=0:numLabels-1
    label = trainingLabels==k;
    %svm训练
    model{k+1} = svmtrain(trainingFeatures,label);%fitcsvm
end
%测试 -训练-集上的准确性
classt=zeros(numImages,numLabels);
for i=1:numLabels
    classt(:,i) = svmclassify(model{i},trainingFeatures); 
end
for j=1:numImages
    p =find(classt(j,:)==1);%找到准确率为1 并 返回下标
    labelt(j).predict=p-1;%临时
end
labeltp=cat(1,labelt.predict);%没有进行多标签处理
labeltp=reshape(labeltp,[numImages/numLabels,numLabels]);%重新调整矩阵 行 列 维
[labelreal,~]=meshgrid(1:numLabels,1:numImages/numLabels);%画出网格
deltp=labeltp-labelreal+1;
%计算正确率
CorrectRate=1-sum(deltp(:))/numImages;

% save my1vsall_model model  
% save('mypara.mat','imgsize','cellSize','hogFeatureSize')  
% end


%预测 
numLabels=10;
testfile='digittest/num/';
% Extract HOG features from the test set
numImages = 11;
for i = 1:numImages
    s = sprintf('num(%d).jpg',i)
    picnames(i,:)=dir([testfile s]);
end
% picnames=dir([testfile 'num(*).jpg']);
numImages=size(picnames,1);
for j=1:numImages
         testset(j).Files=[testfile picnames(j).name];
end 

% numImages=10;
testFeatures = extracthog(testset,numImages,imgsize,cellSize,hogFeatureSize);
%轮流预测
for j=1:numImages
    for k=1:numLabels
       classp(j,k) = svmclassify(model{k},testFeatures(j,:));
    end
    p =find(classp(j,:)==1);
    if ~isempty(p)
        labelp(j).predict=p-1;
    end

end
%将数字存入文件
fid = fopen('digit_result.txt','w');
for i= 1:numImages
    fprintf(fid,'第 %d 位数字：',i);
    prelen = length(labelp(i).predict);
    if prelen == 0
        fprintf(fid,'未识别');
    else
        for j = 1:prelen
            fprintf(fid,' %d',labelp(i).predict(j));
        end
    end
    fprintf(fid,'\r\n');
end
fclose(fid);

end


% fitcecoc uses SVM learners and a 'One-vs-One' encoding scheme.
%fitcsvm用于二分类，fitcecoc用于多分类
%ecoc： Error-Correcting Output Codes
%classifier = fitcecoc(trainingFeatures, trainingLabels);
%training data stored in classifier.X and corresponding class labels stored in classifier.Y.
%Evaluate the Digit Classifier
%genErr = resubLoss(classifier );%Estimate the in-sample classification error.被错误分类的数据占比



function Features=extracthog(imgSet,numImages,imgsize,cellSize,hogFeatureSize)
Features = zeros(numImages, hogFeatureSize, 'single');
for i = 1:numImages
    img = readimage(imgSet, i);
    %图像应该是一样的大小，以保证hog维数一样
    if sum(abs(size(img)-imgsize)) ~= 0
        img = imresize(img,imgsize);%图像大小归一化
    end
    % Apply pre-processing steps
   % img = im2bw(img,graythresh(img));
    Features(i, :) = extractHOGFeatures(img, 'CellSize', cellSize);
    %提取特征并存储
  %  subplot(2,5,i);imshow(img)
end
end

function img = readimage(imgSet, i);
img = imread(imgSet(i).Files);
if size(img,3)>1
   img = rgb2gray(img);
end
end